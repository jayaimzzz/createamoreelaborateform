const userCreateForm = document.getElementById("user-create-form")
const userCreateSubmitButton = userCreateForm.querySelector("button[type='submit']")
const dest = document.getElementById('createdUser')

userCreateForm.addEventListener('submit', createUser)

function createUser(event){
    event.preventDefault();
    let username = document.getElementById('uname').value;
    let email = document.getElementById('email').value;
    let favoriteColor = document.getElementById('favoriteColor').value;
    let ssn = document.getElementById('ssn').value;
    let mobilePhoneNumber = document.getElementById('tel').value;
    let url = document.getElementById('url').value;
    let perferredCommunicationMethod = document.querySelector('input[name="contactMethod"]:checked').value;
    let devices = (Array.from(document.querySelectorAll('input[name="devices"]:checked'))).map(element => element.value);
    let userType = document.getElementById('userType')
    userType = userType.options[userType.selectedIndex].value
    let user = new User({
        username: username,
        email: email,
        favoriteColor: favoriteColor,
        ssn: ssn,
        mobilePhoneNumber: mobilePhoneNumber,
        instagram: url,
        perferredCommunicationMethod: perferredCommunicationMethod,
        devices: devices,
        userType: userType
    })
    
    user = JSON.stringify(user)
    
    sendToServer(user,'/api/user')
    
}

class User {
    constructor(options){
        this.username = options.username;
        this.email = options.email;
        this.favoriteColor = options.favoriteColor;
        this.ssn = options.ssn;
        this.mobilePhoneNumber = options.mobilePhoneNumber;
        this.instagram = options.instagram;
        this.perferredCommunicationMethod = options.perferredCommunicationMethod;
        this.devices = options.devices;
        this.userType = options.userType;
    }
}

function sendToServer(user,url){
    fetch(url,{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: user
    }).then(res => {
        if (res.status === 201){
            display(user);
            userCreateForm.reset(); 
            // alert (`Success`)
        } else if (res.status === 409){
            alert ('Username already taken')
        }
    });
}

function display(user){
    let userObject = JSON.parse(user)
    displayOnPage(`User "${userObject.username}" successfull created`);
    displayOnPage(`Email: ${userObject.email}`);
    displayOnPage(`Favorite Color: ${userObject.favoriteColor}`);
    displayOnPage(`SSN: ***-**-****`)
    displayOnPage(`Mobile phone number: ${userObject.mobilePhoneNumber}`)
    displayOnPage(`Instagram: ${userObject.instagram}`)
    displayOnPage(`Prefferred Communication: ${userObject.perferredCommunicationMethod}`)
    displayOnPage(`Devices: ${userObject.devices}`)
    displayOnPage(`User Type: ${userObject.userType}`)
}
function displayOnPage(text){
    let t = document.createTextNode(text);
    let span = document.createElement('span');
    span.appendChild(t);
    dest.appendChild(span)
    let br = document.createElement('br')
    dest.appendChild(br)
}
