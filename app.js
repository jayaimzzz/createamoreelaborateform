const express = require("express")
const path = require("path")

const app = express()
const publicFolderPath = path.join(__dirname, "public")

app.use(express.json())
app.use(express.static(publicFolderPath))

const users = []

// add POST request listener here
app.post('/api/user', (req, res) => {
    let user = req.body;
    if (users.find(existingUser => existingUser.username === user.username)) {
        console.log(user.username + " already exists")
        res.status(409).send('');
    } else {
        console.log(user.username + " created")
        user.userID = Math.floor(Math.random() * 100000000000)
        users.push(user);
        res.status(201).send('');
    }
})


app.listen(3000);